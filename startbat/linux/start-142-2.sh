#!/bin/bash
source /etc/profile
cd /root/SmallStore/ss-monitor
rm -f monitor-center.jar
cp /tmp/app/monitor-center.jar  .
echo "start /root/SmallStore/ss-monitor"
#./start.sh
sleep 20
cd /root/SmallStore/ss-gateway
rm -f gateway-zuul.jar 
cp /tmp/app/gateway-zuul.jar  .
echo "start /root/SmallStore/ss-gateway"
./start.sh