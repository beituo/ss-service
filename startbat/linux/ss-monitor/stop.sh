#!/bin/bash
pro_patten="monitor-center"
current_pid_list=$(ps -ef | grep $pro_patten | grep -v grep | awk '{print $2}')
for current_pid in $current_pid_list
do
  kill -9 $current_pid
done
