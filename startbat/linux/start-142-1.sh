#!/bin/bash
source /etc/profile
cd /root/SmallStore/ss-register
rm -f register-center.jar
cp /tmp/app/register-center.jar .
echo "start /root/SmallStore/ss-register"
./start.sh
sleep 5
cd /root/SmallStore/ss-config
rm -f config-center.jar 
cp /tmp/app/config-center.jar  .
echo "start /root/SmallStore/ss-config"
./start.sh
sleep 30
cd /root/SmallStore/ss-user
rm -f user-center.jar
cp /tmp/app/user-center.jar .
echo "start /root/SmallStore/ss-user"
./start.sh