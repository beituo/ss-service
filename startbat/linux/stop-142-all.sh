#!/bin/bash
source /etc/profile
cd /root/SmallStore/ss-user
echo "stop /root/SmallStore/ss-user"
./stop.sh
cd /root/SmallStore/ss-gateway
echo "stop /root/SmallStore/ss-gateway"
./stop.sh
cd /root/SmallStore/ss-config
echo "stop /root/SmallStore/ss-config"
./stop.sh
cd /root/SmallStore/ss-monitor
echo "stop /root/SmallStore/ss-monitor"
./stop.sh
cd /root/SmallStore/ss-register
echo "stop /root/SmallStore/ss-register"
./stop.sh