#!/bin/bash
source /etc/profile
cd /root/SmallStore/ss-oauth
rm -f oauth-center.jar 
scp root@douzi7:/tmp/app/oauth-center.jar .
echo "start /root/SmallStore/ss-oauth"
./start.sh

cd /root/SmallStore/ss-file
rm -f file-center.jar
scp root@douzi7:/tmp/app/file-center.jar .
echo "start /root/SmallStore/ss-file"
./start.sh

cd /root/SmallStore/ss-log
rm -f log-center.jar
scp root@douzi7:/tmp/app/log-center.jar .
echo "start /root/SmallStore/ss-log"
./start.sh

cd /root/SmallStore/ss-manage-backend
rm -f manage-backend.jar
scp root@douzi7:/tmp/app/manage-backend.jar .
echo "start /root/SmallStore/ss-manage-backend"
./start.sh
