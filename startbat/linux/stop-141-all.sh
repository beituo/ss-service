#!/bin/bash
source /etc/profile
cd /root/SmallStore/ss-oauth
echo "stop /root/SmallStore/ss-oauth"
./stop.sh
cd /root/SmallStore/ss-file
echo "stop /root/SmallStore/ss-file"
./stop.sh
cd /root/SmallStore/ss-log
echo "stop /root/SmallStore/ss-log"
./stop.sh
cd /root/SmallStore/ss-manage-backend
echo "stop /root/SmallStore/ss-manage-backend"
./stop.sh