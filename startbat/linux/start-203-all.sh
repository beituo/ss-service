#!/bin/bash
source /etc/profile
cd /root/shop-screen/screen-user
rm -f user-center.jar
cp /nas/nas_apk/app/user-center.jar .
echo "start /root/shop-screen/screen-user"
./start.sh
cd /root/shop-screen/screen-roll-channel
rm -f roll-channel-center.jar
cp /nas/nas_apk/app/roll-channel-center.jar .
echo "start /root/shop-screen/screen-roll-channel"
./start.sh
cd /root/shop-screen/screen-oauth
rm -f oauth-center.jar 
cp /nas/nas_apk/app/oauth-center.jar .
echo "start /root/shop-screen/screen-oauth"
./start.sh

