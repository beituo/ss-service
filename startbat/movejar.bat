@echo off
set name=movejar
set cname=移包
title %cname%（%name%）
COLOR A
echo -----------%cname%-----------
cd\
echo 当前目录为%cd%
copy D:\waibuSpace\SmallStore\ss-service\register-center\target\register-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\config-center\target\config-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\file-center\target\file-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\gateway-zuul\target\gateway-zuul.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\log-center\target\log-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\manage-backend\target\manage-backend.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\monitor-center\target\monitor-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\oauth-center\target\oauth-center.jar D:\ssjar\ /y
::copy D:\waibuSpace\SmallStore\ss-service\roll-channel-center\target\roll-channel-center.jar D:\ssjar\ /y
::copy D:\waibuSpace\SmallStore\ss-service\timer-center\target\timer-center.jar D:\ssjar\ /y
copy D:\waibuSpace\SmallStore\ss-service\user-center\target\user-center.jar D:\ssjar\ /y