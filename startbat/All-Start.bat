@echo off

set /p url=请输入项目所在路径（例如：D:\waibuSpace\SmallStore）:
if "%url%"=="" set url=D:\waibuSpace\SmallStore
set pan=%url:~0,2%
echo 您的盘符为：%pan%
echo 您的项目路径为：%url%

set /p install1=请输入是否编译项目(Y/N):
if "%install1%"=="Y" (
	start "" cmd /k call 0-install.bat %pan%,%url%
	TIMEOUT /T 30
)

start "" cmd /k call 1-register.bat %pan%,%url%
start "" cmd /k call 2-config.bat %pan%,%url%
start "" cmd /k call 3-user.bat %pan%,%url%
start "" cmd /k call 4-oauth.bat %pan%,%url%
start "" cmd /k call 5-file.bat %pan%,%url%
start "" cmd /k call 6-log.bat %pan%,%url%
start "" cmd /k call 7-manage-backend.bat %pan%,%url%
start "" cmd /k call 8-monitor.bat %pan%,%url%
start "" cmd /k call 9-zuul.bat %pan%,%url%

::pause
exit