<h1 align="center">ss-service</h1><br/>
<h2>背景</h2> <br/>
这是一个像早餐店的小公众号程序，也可直接访问网络地址；程序分为后台管理程序，手机端程序；后台管理使用小威老师的cloud-service架构，前端使用MiUI构建；
主要完成优惠券展示下载（商家可通过朋友圈分享提供优惠）、商品展示、爬虫养生文章展示等业务；<br/>
<h2>术语</h2>  <br/>
<table border="1">
<tr bgcolor="#DBDBDB"><td>术语</td><td>解释</td></tr>

</table>
<br/>
<h2>模块</h2> <br/>
ss-service<br/>
 ├── config-center    -- 配置中心 [8989]<br/>
 ├── file-center      -- 文件中心 [8994]<br/>
 ├── gateway-zuul     -- 网关中心 [8080]<br/>
 ├── log-center       -- 日志中心 [8995]<br/>
 ├── manage-backend   -- 管理后台 [8996]<br/>
 ├── monitor-center   -- 监控中心 [9001]<br/>
 ├── oauth-center     -- 认证中心 [8998]<br/>
 ├── register-center  -- 注册中心 [8761]<br/>
 ├── user-center      -- 用户中心 [8999]<br/>
 ├── notification-center  -- 通知中心   [8997]<br/>
 ├── service-center       -- 主业务中心（未完成） [8993]<br/>
 └── time-center          -- 定时服务（未完成）
<br/>
<h2>需要组件列表</h2>  <br/>
- jdk 要求1.8，因为用到了lambda表达式。<br/>
- Mysql 5.6或者以上。<br/>
- Redis 3.0以上即可（如果是非本地redis，即redis在别的机器上，那么请注意检查能否远程访问）。<br/>
- Rabbitmq 版本别太低就行。<br/>
- Elasticsearch 日志中心使用，默认日志是存到mysql里，如采用默认配置，可不需要elasticsearch。<br/>
<br/>
<h2>HOST 配置</h2> <br/>
添加如下host配置<br/>
127.0.0.1 api.gateway.com #外网网关ip<br/>
127.0.0.1 local.gateway.com #内网网关ip<br/>
127.0.0.1 local.register.com #注册中心ip<br/>
127.0.0.1 local.monitor.com #监控中心ip<br/>
127.0.0.1 local.mysql.com #数据库ip<br/>
127.0.0.1 local.redis.com #redis ip<br/>
127.0.0.1 local.rabbitmq.com #rabbitmq ip<br/>
<h2>数据库初始化</h2>  <br/>
按照对应中心sql文件夹 建立数据库，并且导入sql，字符集为utf8mb4；<br/>
（user-center、oauth-center、file-center、manage-backend、log-center、notification-center）<br/>
<h2>startbat 启动项目</h2>  <br/>
里边有windows一键脚本，以及linux部署脚本；由于shell的能力太弱，只能写到这样，大家见谅：<br/>
<br/>
start "" cmd /k call 1-register.bat %pan%,%url%<br/>
start "" cmd /k call 2-config.bat %pan%,%url%<br/>
start "" cmd /k call 3-user.bat %pan%,%url%<br/>
start "" cmd /k call 4-oauth.bat %pan%,%url%<br/>
start "" cmd /k call 5-file.bat %pan%,%url%<br/>
start "" cmd /k call 6-log.bat %pan%,%url%<br/>
start "" cmd /k call 7-manage-backend.bat %pan%,%url%<br/>
start "" cmd /k call 8-notification.bat %pan%,%url%<br/>
start "" cmd /k call 9-monitor.bat %pan%,%url%<br/>
start "" cmd /k call 10-zuul.bat %pan%,%url%<br/>
<br/>
请大家按照上边序号执行；其中：notification-center、monitor-center都可以不启动；<br/>
<p />
[回到顶部](#readme)

