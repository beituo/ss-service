package com.cloud.oauth.config;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 开启session共享
 * 
 * @author douzi douzaixing2008@163.com
 *
 */
@EnableRedisHttpSession
public class SessionConfig {

}
